#package specific information for the desktop file
sed -i "/^Terminal=/i Icon=share/gedit/logo/gedit-logo.png" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/gedit is a text editor/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
